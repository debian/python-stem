## Stem (Python Tor Library)

**NOTE**: Stem is mostly unmaintained. However, you can still:

* Open issues at <https://github.com/torproject/stem/issues>
* Work on an issue and open a pull request at <https://github.com/torproject/stem/pulls>
* Contact us (via tor-dev mailing list or gk at torproject dot org) to request
    a new bugfix release including some patches in the Stem's `master` branch or
    pull requests.

Stem is a Python controller library for **[Tor](https://www.torproject.org/)**. With it you can use Tor's [control protocol](https://gitweb.torproject.org/torspec.git/tree/control-spec.txt) to script against the Tor process, or build things such as [Nyx](https://nyx.torproject.org/).

Documentation and tutorials available at **[stem.torproject.org](https://stem.torproject.org/)**.
