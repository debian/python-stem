Source: python-stem
Section: python
Priority: optional
Maintainer: Ulises Vitulli <dererk@debian.org>
Uploaders: Federico Ceratto <federico@debian.org>
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: dh-python,
 python3-all,
 python3-sphinx,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://gitweb.torproject.org/stem.git
Vcs-Git: https://salsa.debian.org/debian/python-stem.git
Vcs-Browser: https://salsa.debian.org/debian/python-stem
Testsuite: autopkgtest-pkg-python

Package: python3-stem
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Replaces: python-stem (<< 1.0.1-3)
Breaks: python-stem (<< 1.0.1-3)
Description: Tor control library for Python 3 series
 Stem is a Python controller library for Tor. With it you can use
 Tor's control protocol to script against the Tor process and read
 descriptor data relays publish about themselves.
 .
 This is Python 3 series module.

Package: python3-stem-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
 libjs-jquery,
 libjs-underscore
Conflicts: python3-stem (<< 1.8.0-1)
Description: Tor control library for Python 3 - documentation
 Stem is a Python controller library for Tor. With it you can use
 Tor's control protocol to script against the Tor process and read
 descriptor data relays publish about themselves.
 .
 This package contains documentation for developers
